#pragma once
#include "PointCloud.h"


class PointCloudFilter
{
public:


	///<summary>Constructor of the PointCloudFilter</summary>
	PointCloudFilter() {};

	///<summary>Pure virtual filter function</summary>
	///<param name="pc">This parameter takes a pointcloud adress to filter it</param>
	virtual void filter(PointCloud& pc) = 0 {};
};

