#include "PassThroughFilter.h"

void PassThroughFilter::setUpperLimitX(double ulx)
{
	upperLimitX = ulx;
}

void PassThroughFilter::setUpperLimitY(double uly)
{
	upperLimitY = uly;
}

void PassThroughFilter::setUpperLimitZ(double ulz){
	upperLimitZ = ulz;
}

void PassThroughFilter::setLowerLimitX(double llx)
{
	lowerLimitX = llx;
}

void PassThroughFilter::setLowerLimitY(double lly)
{
	lowerLimitY = lly;
}

void PassThroughFilter::setLowerLimitZ(double llz)
{
	lowerLimitZ = llz;
}

double PassThroughFilter::getUpperLimitX()
{
	return upperLimitX;
}

double PassThroughFilter::getUpperLimitY()
{
	return upperLimitY;
}

double PassThroughFilter::getUpperLimitZ()
{
	return upperLimitZ;
}

double PassThroughFilter::getLowerLimitX()
{
	return lowerLimitX;
}

double PassThroughFilter::getLowerLimitY()
{
	return lowerLimitY;
}

double PassThroughFilter::getLowerLimitZ()
{
	return lowerLimitZ;
}

void PassThroughFilter::filter(PointCloud& pc)
{
	int pn = pc.getPointnumber();
	int i = 0;
	while( i < pn) {
		Point tmp;
		tmp = pc.getPoint(i);
		if (tmp.getX()>upperLimitX||tmp.getX()<lowerLimitX||tmp.getY()>upperLimitY||tmp.getY()<lowerLimitY|| tmp.getZ()>upperLimitZ||tmp.getZ()<lowerLimitZ){
			pc.deletePoint(i);
			pn = pc.getPointnumber();
		
		}
		else {
			i++;
		}
	}
}
