#pragma once

#include "Creator.h"
#include <iostream>


class PointCloudInterface
{
private:
	PointCloud pointCloud;
	PointCloud patch;
	vector<PointCloudGenerator*> generators;
	PointCloudRecorder* recorder;

public:

	PointCloudInterface() { std::cout << "Initizaling the program"; };


	///<summary>addGenerator</summary>
	///<param name="PointCloudGenerator *">This parameter will be added to the PointCloudGenerator vector</param>
	void addGenerator(PointCloudGenerator*);

	


	///<summary>setRecorder</summary>
	///<param name="PointCloudRecorder *">This parameter will be setted as current recorder</param>
	void setRecorder(PointCloudRecorder*);


	///<summary>Set capture</summary>
	bool generate();

	///<summary>Set capture</summary>
	bool record();

	///<summary>SetUp</summary>
	bool setUp(Creator crt);


};




