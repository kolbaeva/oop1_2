#pragma once
#include <Eigen/Dense>
#include "PointCloud.h"
class Transform
{
private:

	Eigen::Vector3d angles;
	Eigen::Vector3d trans;
	Eigen::Matrix4d transMatrix;


public:
	///<summary>Constructor of PassThroughFilter object</summary>
	///<param name="tx">This parameter takes a double value to initialize trans[0]</param>
	///<param name="ty">This parameter takes a double value to initialize trans[1]</param>
	///<param name="tz">This parameter takes a double value to initialize trans[2]</param>
	///<param name="rx">This parameter takes a double value to initialize angles[0]</param>
	///<param name="ry">This parameter takes a double value to initialize angles[1]</param>
	///<param name="rz">This parameter takes a double value to initialize angles[2]</param>
	Transform(double tx, double ty, double tz, double rx, double ry, double rz);

	///<summary>Set Rotation</summary>
	///<param name="a[]">This parameter takes a vector to assign it to angles</param>
	void SetRotation(double a[]);

	void SetRotation(Eigen::Vector3d ang);

	void SetTranslation(Eigen::Vector3d tr);


	///<summary>Set Translation</summary>
	///<param name="a[]">This parameter takes a vector to assign it to trans</param>
	void SetTranslation(double t[]);

	///<summary>Translate and Rotate the point p by trans and angles values </summary>
	///<param name="p">Takes a Point to doTransform</param>
	///<return>Return the Transformed Point</return>
	Point doTransform(Point p);

	///<summary>Translate and Rotate the PointCloud points by trans and angles values </summary>
	///<param name="pc">Takes a PointCloud to doTransform</param>
	///<return>Return the Transformed PointCloud</return>
	PointCloud doTransform(PointCloud pc);
	
	void configure();
};

