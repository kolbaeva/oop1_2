#pragma once
#include "PointCloudFilter.h"
#include <vector>
#include <iterator>

using namespace std;

class FilterPipe{
private:
	vector<PointCloudFilter*> filters;

public:

	///<summary>Constructor of the FilterPipe object</summary>
	FilterPipe() {};

	FilterPipe(PointCloudFilter* ptf, PointCloudFilter* rof);

	///<summary>The function to add a new filter</summary>
	void addFilter(PointCloudFilter*);

	///<summary>The function to filterOut a PointCloud</summary>
	///<param name="pc">Parameter that takes a PointCloud adrress to filter</param>
	void filterOut(PointCloud& pc);

};

