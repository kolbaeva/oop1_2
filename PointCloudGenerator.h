#pragma once
#include "Transform.h"
#include "FilterPipe.h"


class PointCloudGenerator{

protected:
	Transform transform;
	FilterPipe* filterPipe;
public:

	///<summary>Constructor of the PointCloudGenertor</summary>
	///<param name="trn">Parameter that takes a transform object</param>
	///<param name="fp">Parameter that takes a filterpipe pointer</param>
	PointCloudGenerator(Transform trn, FilterPipe* fp) :transform(trn), filterPipe(fp) {};

	///<summary>Set capture</summary>
	virtual PointCloud capture() = 0;

	///<summary>Set captureFor</summary>
	virtual PointCloud captureFor() = 0;

	///<summary>Set FilterPipe/summary>
	///<param name="FilterPipe *">This parameter takes a vector to set current filterPipe with given</param>
	void setFilterPipe(FilterPipe*);
};
