#include "RadiusOutlierFilter.h"
//Set radius
void RadiusOutlierFilter::setRadius(double radius) {
	this->radius = radius;
}

//Get radius
double RadiusOutlierFilter::getRadius() {
	return radius;
}

//Filter
void RadiusOutlierFilter::filter(PointCloud& pc) {
	//for every point check is there another point in radius
	int radiuscheck=0;
	int i = 0;
	int j = 1;
	int pn = pc.getPointnumber();
	while (i < pn) {
		while (j < pn) {

			if (i!=j &&pc.getPoint(i).distance(pc.getPoint(j)) < radius) {
				j = 0;
				radiuscheck++;
				break;
			}
			else {
				j++;

			}
			
		}
		if (radiuscheck == 0) {
			
				pc.deletePoint(i);
			
			
		}
		else {
			radiuscheck = 0;
			i++;
		}
		j = 0;
	
	}

}