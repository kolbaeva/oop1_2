#include "FilterPipe.h"

FilterPipe::FilterPipe(PointCloudFilter* ptf, PointCloudFilter* rof){
	addFilter(ptf);
	addFilter(rof);
}

void FilterPipe::addFilter(PointCloudFilter* pcf){
	filters.push_back(pcf);
}

void FilterPipe::filterOut(PointCloud& pc){
	for ( int i= 0; i <filters.size(); i++){
		filters[i]->filter(pc);

	}

}
