#include "Creator.h"




PointCloudFilter* Creator::CreatePTF(double ux, double lx, double uy, double ly, double uz, double lz)
{
	return new PassThroughFilter(ux, lx, uy, ly, uz, lz);
}

PointCloudFilter* Creator::CreateROF(double rad){
	return new RadiusOutlierFilter(rad);
}

PointCloudGenerator* Creator::CreatePCG(string type, string filename, Transform trn, FilterPipe* fp)
{
	if (type == "dc")
	{
		return new DepthCamera(filename,trn,fp);
	}
}

FilterPipe* Creator::CreateFP(PointCloudFilter* a, PointCloudFilter* b)
{

	return new FilterPipe(a,b);
}

PointCloudRecorder* Creator::CreateRec(string fn)
{
	return new PointCloudRecorder(fn);
}

Transform* Creator::CreateTRN(double tx, double ty, double tz, double ax, double ay, double az)
{
	return new Transform(tx,ty,tz,ax,ay,az);
}
