#include "Point.h"
#include <math.h>

//set x
void Point::setX(double _x ) {
	x = _x;
}

//set y
void Point::setY(double _y) {
	y = _y;
}

//set z
void Point::setZ(double _z) {
	z = _z;
}

//get x
double Point::getX() {
	return x;
}

//get y
double Point::getY() {
	return y;
}

//get z
double Point::getZ() {
	return z;
}
//operator +
Point Point::operator +(const Point& p)  {
	Point tmp(x + p.x,y+p.y, z+p.z);
	return tmp;
}
//operator ==
bool Point::operator ==( Point p)  {
	if (distance(p)<0.35) {
		return true;
	}
	else { return false; }
}





double Point::distance( Point p) 
{
	return pow(pow(x - p.x, 2.0) + pow(y - p.y, 2.0) + pow(z - p.z, 2.0), 0.5);
}
