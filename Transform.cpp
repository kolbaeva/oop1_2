#include "Transform.h"
#include <math.h>
#include <iostream>
#define PI 3.14159265

///Warning! The camera's coordinate system already transformed from the base coordinate system when giving translation
///amounts and angles one should be consider it and give the negatives of the transformed system 
Transform::Transform(double tx, double ty, double tz, double ax, double ay, double az) {
	angles << ax, ay, az;
	trans << tx, ty, tz;
	this->configure();

}

void Transform::SetRotation(double a[]){
	angles << a[0], a[1], a[2];
	
	this->configure();


}

void Transform::SetRotation(Eigen::Vector3d ang){
	angles = ang;
	this->configure();
}

void Transform::SetTranslation(Eigen::Vector3d tr)
{
	trans =tr;
	this->configure();
}

void Transform::SetTranslation(double t[])
{
	trans << t[0], t[1], t[2];
	
}
	

//dotransform
Point Transform::doTransform(Point p) {
	Point tmp;
	Eigen::Vector4d pnt;
	pnt << p.getX(), p.getY(), p.getZ(), 1;
	pnt = transMatrix * pnt;

	
	tmp.setX(pnt[0]);
	tmp.setY(pnt[1]);
	tmp.setZ(pnt[2]);
	return tmp;
}

//dotransformforpc
PointCloud Transform::doTransform(PointCloud pc) {
	list<Point>::iterator it;
	PointCloud tmppc;

	for (it=pc.points.begin(); it!=pc.points.end(); ++it) {
		Point tmp = doTransform(*it);
		
		tmppc.addPoint(tmp);
	
	}
	
	return tmppc;
}

void Transform::configure()
{
	Eigen::Matrix4d It;
	Eigen::Matrix4d Ix;
	Eigen::Matrix4d Iy;
	Eigen::Matrix4d Iz;

	It << 1, 0, 0, trans[0],
		0, 1, 0, trans[1],
		0, 0, 1, trans[2],
		0, 0, 0, 1;

	Ix << 1, 0, 0, 0,
		0, cos(angles[0]), -sin(angles[0]), 0,
		0, sin(angles[0]), cos(angles[0]), 0,
		0, 0, 0, 1;

	Iy << cos(angles[1]), 0, sin(angles[1]), 0,
		0, 1, 0, 0,
		-sin(angles[1]), 0, cos(angles[1]), 0,
		0, 0, 0, 1;

	Iz << cos(angles[2]), -sin(angles[2]), 0, 0,
		sin(angles[2]), cos(angles[2]), 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1;

	transMatrix = It;
	transMatrix = transMatrix * Ix;
	transMatrix = transMatrix * Iy;
	transMatrix = transMatrix * Iz;
}

