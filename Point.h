#pragma once
class Point
{
private:

	double x, y, z;

public:
	///<summary>Constructor of Point object</summary>
	///<param name="_x">This parameter takes a double value to initialize x value</param>
	///<param name="_y">This parameter takes a double value to initialize y value</param>
	///<param name="_z">This parameter takes a double value to initialize z value</param>
	Point(double _x=0, double _y=0, double _z=0) :x(_x), y(_y), z(_z) { };

	///<summary>Set the x with parameter</summary>
	///<param name ="_x">This parameter takes a double value to set x value</param>
	void setX(double _x);

	///<summary>Set the y with parameter</summary>
	///<param name ="_y">This parameter takes a double value to set y value</param>
	void setY(double _y);

	///<summary>Set the z with parameter</summary>
	///<param name ="_z">This parameter takes a double value to set z value</param>
	void setZ(double _z);

	///<summary>Return to x</summary>
	///<return>Return double value from x</return>
	double getX();

	///<summary>Return to y</summary>
	///<return>Return double value from y</return>
	double getY();

	///<summary>Return to z</summary>
	///<return>Return double value from z</return>
	double getZ();

	///<summary>Adding point</summary>
	Point operator +(const Point& p);

	///<summary>== operator that check the distance between two points </summary>
	///<param name="p">Second point to compare the point</param>
	///<return>Return true if 0.35>distance(p)</return>
	///<return>Return false if distance(p)>0.35</return>
	bool operator ==( Point p) ;

	///<summary>Calculate the distance between two points</summary>
	///<return>Return to double value between two points</return>


	double distance( Point p) ;
};

