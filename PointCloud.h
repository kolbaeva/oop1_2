#pragma once
#include "Point.h"
#include <iterator>
#include <list>
using namespace std;

class PointCloud
{
	friend class Transform;

	list<Point> points;


public:


	void addPoint(Point p);

	///<summary>Set the n-1 element of the list with a point p </summary>
	///<param name ="n">This parameter takes an integer value to element which will be seted</param>
	///<param name="p">This parameter takes a point reference to set it to n-1th element</param>
	void setPoint(int n,  Point p);

	///<summary>Get point</summary>
	///<param name="n">This parameter is the place of the returning number</param>
	Point getPoint(int n);

	///<summary>Return pointNumber</summary>
	///<return>Return to double value of pointNumber</return>
	int getPointnumber();

	///<summary>= operator to assign a point cloud<summary>
	///<param name"pc">This parameter takes a pointCloud reference to assign</param>
	void operator = ( PointCloud pc);

	///<summary>+ Operator to add two point cloud</summary>
	///<param name="pc">This parameter takes a pointcloud reference to add two point cloud</param>
	///<return>Return to sum of two point cloud</return>
	void operator + ( PointCloud);

	///<summary>Delete a point</summary>
	///<param name="n"> Delete the nth point in the list</param>
	void deletePoint(int n);


};

