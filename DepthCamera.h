#pragma once
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include "PointCloud.h"
#include "PointCloudGenerator.h"
using namespace std;

class DepthCamera :public PointCloudGenerator{
private:
	string filename;
public:
	///<summary>Constructor of Depthamera Object</summary>
	///<param name="_filename">This parameter takes a string value to initialize the filename</param>
	DepthCamera(string _filename,Transform trn,FilterPipe* fp) : filename(_filename),PointCloudGenerator(trn,fp) {};

	///<summary>Set filename with parameter</summary>
	///<param name="_filename">This parameter takes a string value to assign it to filename</param>
	void setFile(string _filename);

	///<summary>Get filename</summary>
	///<return>Return to string value of filename</return>
	string getFile();

	///<summary>Read points in the file and set those to a point cloud</summary>
	PointCloud capture();


	///<summary>Read the points in the file, set those to a point cloud,filter and transform PointCloud</summary>
	PointCloud captureFor();

};
