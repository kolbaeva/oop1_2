#include "DepthCamera.h"

//set filename
void DepthCamera::setFile(string filename) {
	this->filename = filename;
}

//get filename
string DepthCamera::getFile() {
	return filename;
}


//capture
PointCloud DepthCamera::capture() {
	fstream file;
	file.open(filename);
	string line;
	PointCloud tmp;

	int i = 0;
	while (getline(file, line)) {

		stringstream ss(line);
		double x, y, z;
		ss >> x >> y >> z;
		Point tmp2(x, y, z);
	
		tmp.addPoint(tmp2);

		i++;
	}
	file.close();
	cout << "reading is succcesfull" << endl;
	return tmp;
}


//capturefor
PointCloud DepthCamera::captureFor(){
	PointCloud tmp=capture();
	this->filterPipe->filterOut(tmp);
	tmp = transform.doTransform(tmp);
	return tmp;
}
