#include "PointCloudRecorder.h"


//set filename
void PointCloudRecorder::setFilename(string filename) {
	this->filename = filename;
}

string PointCloudRecorder::getFilename()
{
	return filename;
}

bool PointCloudRecorder::save(PointCloud& pc)
{
	ofstream file;
	file.open(filename);
	char ch = ' ';
	if (file.is_open()) {

		for (int i = 0; i < pc.getPointnumber(); i++) {
			file << pc.getPoint(i).getX() << ch << pc.getPoint(i).getY() << ch << pc.getPoint(i).getZ() << endl;

		}
		file.close();
		std::cout << "record is successfull" << endl;
		return true;

	}
	else {
		return false;
	}

}
