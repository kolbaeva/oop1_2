#pragma once
#include "PointCloudFilter.h"

class RadiusOutlierFilter :public PointCloudFilter
{
private:
	double radius;

public:

	///<summary>Constructor of PointCloudRecorder object</summary>
	///<param name="_radius">This parameter takes a double value to initialize the radius</param>
	RadiusOutlierFilter(double _radius) :radius(_radius) {};

	///<summary>Set radius with parameter</summary>
	///<param name="_radius">This parameter take a double value to assign to radius</param>
	void setRadius(double _radius);

	///<summary>Get radius</summary>
	///<return>Return to double value of radius</return>
	double getRadius();

	///<summary>Filter the points in the PointClouds if there is not such a neighbor point inside radius</summary>
	///<param name="pc">This parameter takes a reference of PointCloud to filter</param>
	void filter(PointCloud& pc);
};

