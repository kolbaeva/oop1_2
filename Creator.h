#pragma once
#include "PointCloudGenerator.h"
#include "DepthCamera.h"
#include "FilterPipe.h"
#include "PassThroughFilter.h"
#include "RadiusOutlierFilter.h"
#include "PointCloudRecorder.h"
#include "Transform.h"
class  Creator{

public:

	Creator() {};

	static PointCloudFilter* CreatePTF(double ux, double lx, double uy, double ly, double uz, double lz);

	static PointCloudFilter* CreateROF(double rad);

	static PointCloudGenerator* CreatePCG(string type, string filename,Transform trn, FilterPipe* fp);

	static FilterPipe* CreateFP(PointCloudFilter* a, PointCloudFilter* b);

	static PointCloudRecorder* CreateRec(string fn);

	static Transform* CreateTRN(double tx, double ty, double tz, double ax, double ay, double az);
};

