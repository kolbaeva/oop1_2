#include "PointCloudInterface.h"
#define PI 3.14159265

void PointCloudInterface::addGenerator(PointCloudGenerator *generator){
    generators.push_back(generator);
	std::cout << "generator added"<<endl;
}

void PointCloudInterface::setRecorder(PointCloudRecorder *_recorder){
    recorder = _recorder;
	std::cout << "Recorder set to "<<recorder->getFilename()<< endl;

}

bool PointCloudInterface::generate(){
	if (generators.size() > 0) {
		for (int i = 0; i < generators.size(); i++) {
			this->pointCloud= generators[i]->captureFor();
			this->patch+(this->pointCloud);
		}
		std::cout << "Generating PointCloud" << endl;
		return true;
	}
	else {
		std::cout << "something went wrong"<<endl;
		return false;
	}

}

bool PointCloudInterface::record(){
	
	return recorder->save(this->patch);


}

bool PointCloudInterface::setUp(Creator crt){
	setRecorder(crt.CreateRec("Output.txt"));
	Transform tr1(-100, -500, -50, 0, 0, 0.5 * PI);
	Transform tr2(-550, -150, -50, 0, 0, -0.5 * PI);
	cout << "adding files to program" << endl;
	addGenerator(crt.CreatePCG("dc", "camera1.txt", tr1, crt.CreateFP(crt.CreatePTF(400, 0, 400, 0, 45, -45), crt.CreateROF(25))));
	addGenerator(crt.CreatePCG("dc", "camera2.txt", tr2, crt.CreateFP(crt.CreatePTF(500, 0, 500, 0, 45, -45), crt.CreateROF(25))));
	cout << "generating..." << endl;
	generate();
	cout << "recording..." << endl;
	record();
	cout << "Done!";
	

	return false;
}


