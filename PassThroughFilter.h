#pragma once
#include "PointCloudFilter.h"

class PassThroughFilter:public PointCloudFilter{
private:
	double upperLimitX;
	double lowerLimitX;
	double upperLimitY;
	double lowerLimitY;
	double upperLimitZ;
	double lowerLimitZ;
public:
	///<summary>Constructor of PassThroughFilter object</summary>
	///<param name="ux">This parameter takes a double value to initialize upperLimitX</param>
	///<param name="uy">This parameter takes a double value to initialize upperLimitY</param>
	///<param name="uz">This parameter takes a double value to initialize upperLimitZ</param>
	///<param name="lx">This parameter takes a double value to initialize lowerLimitX</param>
	///<param name="ly">This parameter takes a double value to initialize lowerLimitY</param>
	///<param name="lz">This parameter takes a double value to initialize lowerLimitZ</param>
	PassThroughFilter(double ux, double lx, double uy, double ly, double uz, double lz) :upperLimitX(ux), lowerLimitX(lx), upperLimitY(uy), lowerLimitY(ly), upperLimitZ(uz), lowerLimitZ(lz) {};
	
	///<summary>Set upperLimitX</summary>
	///<param name="ux">This parameter takes a double value to assign it to upperLimitX</param>
	void setUpperLimitX(double ux);

	///<summary>Set upperLimitY</summary>
	///<param name="uy">This parameter takes a double value to assign it to upperLimitY</param>
	void setUpperLimitY(double uy);

	///<summary>Set upperLimitZ</summary>
	///<param name="uz">This parameter takes a double value to assign it to upperLimitZ</param>
	void setUpperLimitZ(double uz);

	///<summary>Set lowerLimitY</summary>
	///<param name="lx">This parameter takes a double value to assign it to lowerLimitX</param>
	void setLowerLimitX(double lx);

	///<summary>Set upperLimitY</summary>
	///<param name="uy">This parameter takes a double value to assign it to upperLimitY</param>
	void setLowerLimitY(double ly);

	///<summary>Set lowerLimitZ</summary>
	///<param name="lz">This parameter takes a double value to assign it to lowerLimitZ</param>
	void setLowerLimitZ(double lz);

	///<summary>Get upperLimitX</summary>
	///<return>return to double value of upperLimitX</return>
	double getUpperLimitX();

	///<summary>Get upperLimitY</summary>
	///<return>return to double value of upperLimitY</return>
	double getUpperLimitY();

	///<summary>Get upperLimitZ</summary>
	///<return>return to double value of upperLimitY</return>
	double getUpperLimitZ();

	///<summary>Get upprLimitX</summary>
	///<return>return lowerLimitX value</return>
	double getLowerLimitX();

	///<summary>Get lowerLimitY</summary>
	///<return>return to double value of  lowerLimitY </return>
	double getLowerLimitY();

	///<summary>Get lowerLimitZ</summary>
	///<return>return to double value of  lowerLimitZ </return>
	double getLowerLimitZ();

	///<summary>Filter Pointcloud and delete any points outside the limits</summary>
	///<param name="pc">This parameter takes a PointCloud reference to filter</param>
	void filter(PointCloud& pc);
};

