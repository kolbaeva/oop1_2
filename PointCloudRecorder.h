#pragma once
#include<string>
#include <fstream>
#include <sstream>
#include "PointCloud.h"
#include <iostream>
using namespace std;

class PointCloudRecorder
{
private:
	string filename;

public:
	///<summary>Constructor of PointCloudRecorder object</summary>
	///<param name="_filename">This parameter takes a string value to initialize the filename</param>
	PointCloudRecorder(string _filename) :filename(_filename) { };

	///<summary>Set filename with parameter</summary>
	///<param name="_filename">This parameter takes a string value to assign it to filename</param>
	void setFilename(string _filename);

	///<summary>Get filename</summary>
	///<return>Return to string value of filename</return>
	string getFilename();

	///<summary>Save points in the file from a PointCloud</summary>
	///<param name="pc">This parameter takes a PointCloud reference to save it to file</param>
	///<return>Return true value if file is accesible,else retrun false</return>
	bool save(PointCloud& pc);

};

